package globals

import (
	"emperror.dev/emperror"
	"f4b.io/you-go-follow/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/nanobox-io/golang-scribble"
	"github.com/thoas/go-funk"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"path"

	"github.com/sirupsen/logrus"

	logrushandler "emperror.dev/handler/logrus"
)

// Log is the logger
var Log = &logrus.Logger{
	Out: os.Stdout,
	Formatter: &logrus.TextFormatter{
		//TimestampFormat: "2018-10-03 20:13:37",
		FullTimestamp: true,
	},
	Hooks: make(logrus.LevelHooks),
	Level: logrus.DebugLevel,
}

// Handler is the Handler
var Handler = emperror.WithDetails(logrushandler.New(Log))

// UserName UserName
var UserName string

// Password Password
var Password string

// DryRun DryRun
var DryRun bool

// UseProxy UseProxy
var UseProxy bool

// Silent Silent
var Silent bool

// RefreshTargets Refresh Targets (or use the ones already in db)
var RefreshTargets bool

// Runs Runs
var Runs int

// InfluencerUserNames InfluencerUserNames
var InfluencerUserNames []string

// ConfigFile from where Sounds are being loaded
var ConfigFile string

// SoundNames List of possible Sound Files
var errorCodesMap = map[int]string{
	0:    "error: '%s'",
	1337: "error: '%s'",
}

func GetStorage(dbName string) *gorm.DB {
	db, err := gorm.Open("sqlite3", path.Join(DefaultDbDirectory, dbName+".db"))
	if err != nil {
		Log.Error("Error getting db:", err)
	}
	//defer db.Close()
	// Migrate the schema
	db.AutoMigrate(
		&models.InstaUser{},
	)

	return db
}

func GetScribbleDb(dbName string) *scribble.Driver {
	db, err := scribble.New(DefaultDbDirectory, nil)
	if err != nil {
		Log.Error("Error getting db:", err)
	}
	CheckError(err, 0)

	return db
}

func GetMongoDb(dbName string) *mongo.Database {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://root:root@localhost:27017"))
	if err != nil {
		Log.Error("Error getting mongo db:", err)
	}

	db := client.Database(dbName)

	return db
}

// CheckError is a generic function to validate all errors.
func CheckError(err error, code int) {
	if err != nil {
		Log.Fatalf(errorCodesMap[code], err.Error())

		if code > 0 && code != 1337 {
			Log.Fatalln("--- exiting ---")
			os.Exit(code)
		} else {

		}
	}
}

// CheckErrorAndExit run CheckError with default parameters (code=1337, exit-if-error)
func CheckErrorAndExit(err error) {
	if err != nil {
		Handler.Handle(err)
	}
	CheckError(err, 1337)
}

// MakeUncertain return an "uncertain" maxTargetCount (try to make it more human)
func MakeUncertain(inp int) int {
	return inp + funk.RandomInt(1, inp)
}
