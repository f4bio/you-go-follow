package globals

// UseDb specifies which db to use
const UseDb = "scribble"

// DefaultConfigFile default path to config file
const DefaultConfigFile = "./config.json"

// DefaultUseProxy default path to config file
const DefaultUseProxy = false

// DefaultRefreshTargets default is dont refresh at every start
const DefaultRefreshTargets = false

// DefaultDryRun default path to config file
const DefaultDryRun = false

// DefaultSilent default path to config file
const DefaultSilent = false

// DefaultRuns default path to config file
const DefaultRuns = 5

// DefaultWorkingDirectory specifies
const DefaultWorkingDirectory = "./tmp"

// LogDirectory specifies
const LogDirectory = "./logs"

// SessionFile specifies
const SessionFile = "session"

// DbDirectory specifies
const DefaultDbDirectory = "db"

// DefaultDbName specifies
const DefaultDbName = "yougofollow"

// DbCollectionWorking specifies
const DbCollectionWorking = "_working"

// DbCollectionTargets specifies
const DbCollectionTargets = "targets"

// DbCollectionUsers specifies
const DbCollectionUsers = "users"

// MaxFollowingsCount specifies
const MaxFollowingsCount = 100

// ProxyList specifies
var ProxyList = []string{
	"socks5://x8233405:Tfc8exbRdZ@proxy-nl.privateinternetaccess.com:1080",
	"socks5://195.201.36.98:1080",
	"socks5://149.249.0.210:1080",
	"socks5://78.47.225.59:9050",
	"socks5://207.154.231.216:1080",
	"socks5://145.239.0.192:1080",
	"socks5://207.154.231.209:1080",
	"socks5://46.101.225.83:1080",
	"socks5://207.154.231.208:1080",
	"socks5://165.227.139.133:1080",
	"socks5://46.101.240.221:1080",
	"socks5://88.198.50.103:1080",
	"socks5://207.154.231.213:1080",
	"socks5://88.198.24.108:1080",
	"socks5://195.201.36.98:1080",
	"socks5://148.251.238.35:1080",
	"socks5://207.154.231.211:1080",
	"socks5://139.59.207.66:1080",
	"socks5://144.76.169.123:8080",
	"socks5://88.99.14.147:41630",
	"socks5://207.154.231.212:1080",
	"socks5://176.9.75.42:1080",
	"socks5://176.9.119.170:1080",
	"socks5://136.243.43.126:9999",
	"socks5://138.68.67.164:1081",
	"socks5://46.4.96.137:1080",
	"socks5://207.154.231.210:1080",
	"socks5://207.154.231.217:1080",
	"socks5://46.5.252.70:1080",
	"socks5://185.108.76.34:9050",
	"socks5://88.99.192.97:31904",
	"socks5://46.4.88.215:9050",
	"socks5://195.201.129.31:9050",
	"socks5://46.4.88.203:9050",
	"socks5://78.47.225.59:9050",
	"socks5://46.4.88.220:9050",
}
var Distribution = []int{1, 1, 1, 2, 2, 3, 4, 5, 4, 3, 2, 2, 1, 1, 1}
