package main

import "f4b.io/you-go-follow/cmd"

/*
  Usage:  $ ygf --user <USERNAME> --password <PASSWORD> --influencer <INFLUENCER_USERNAME> [--influencer <INFLUENCER_USERNAME>...]

  ex.: $ ygf --username myUsername --password myPassword --influencer somebody1sUsername --influencer somebody2sUsername --influencer somebody3sUsername
*/
func main() {
	// CPU profiling by default
	//defer profile.Start(profile.CPUProfile).Stop()
	// Memory profiling
	//defer profile.Start(profile.MemProfile).Stop()
	cmd.Execute()
}
