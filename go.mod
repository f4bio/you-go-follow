module f4b.io/you-go-follow

go 1.14

require (
	emperror.dev/emperror v0.32.0
	emperror.dev/errors v0.7.0
	emperror.dev/handler/logrus v0.3.1
	github.com/ahmdrz/goinsta/v2 v2.4.5
	github.com/aws/aws-sdk-go v1.15.55
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20190309225732-aa3e7c118975
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v0.0.7
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/tcnksm/go-input v0.0.0-20180404061846-548a7d7a8ee8
	github.com/thoas/go-funk v0.6.0
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.3.1
	go.uber.org/multierr v1.5.0 // indirect
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200406155108-e3b113bbe6a4 // indirect
	golang.org/x/tools v0.0.0-20200406213809-066fd1390ee0 // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
	honnef.co/go/tools v0.0.1-2020.1.3 // indirect
)
