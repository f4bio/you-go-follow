package models

import (
	"github.com/ahmdrz/goinsta/v2"
	"github.com/jinzhu/gorm"
	"time"
)

type InstaUser struct {
	gorm.Model
	goinsta.User
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
