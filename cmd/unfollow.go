package cmd

import (
	"f4b.io/you-go-follow/globals"
	"f4b.io/you-go-follow/models"
	"github.com/ahmdrz/goinsta/v2"
	"github.com/jinzhu/gorm"
)

// GoUnfollow does something
func GoUnfollow(insta *goinsta.Instagram) error {
	globals.Log.Println("[UNFOLLOW]: going to unfollow targets")

	//var err error
	var db *gorm.DB
	var targets []models.InstaUser
	db = globals.GetStorage("working")

	db.Find(&targets)

	for targetUser := range targets {
		if globals.DryRun != true {
			globals.Log.Info("[UNFOLLOW]: going to unfollow:", targetUser)
			//err = targetUser.Follow()
			//if err != nil {
			//	globals.Log.Fatalln(err)
			//	continue
			//}

		} else {
			globals.Log.Info("[UNFOLLOW/DRY RUN MODE]: skipping unfollowing of:", targetUser)
		}
		WaitSomeSeconds()
	}

	return nil
}
