package cmd

import (
  "f4b.io/you-go-follow/globals"
)

var Log = globals.Log

func CheckErrorAndExit(err error) {
  globals.CheckErrorAndExit(err)
}
