package cmd

import (
	"f4b.io/you-go-follow/globals"
	"io"
	"io/ioutil"
	"os"

	"github.com/ahmdrz/goinsta/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/thoas/go-funk"
)

var rootCmd = &cobra.Command{
	Use:     "you-go-follow",
	Aliases: []string{"ygf"},
	Short:   "you-go-follow is a instagram followers generator (real followers)",
	Long: `A real
            followers
            generator
            bot`,
	Run: func(cmd *cobra.Command, args []string) {
		var err error
		loopCount := 0
		workingDirectory, err := ioutil.TempDir("", "ygf")
		if err != nil || workingDirectory == "" {
			workingDirectory = globals.DefaultWorkingDirectory
		}
		Log.Info("workingDirectory:", workingDirectory)

		// create instagram object
		insta := goinsta.New(globals.UserName, globals.Password)

		if globals.DryRun {
			shuffledProxies := funk.ShuffleString(globals.ProxyList)
			err = insta.SetProxy(shuffledProxies[0], false)
			globals.CheckError(err, 0)
			globals.Log.WithFields(logrus.Fields{
				"proxy": shuffledProxies[0],
			}).Infof("*** USING PROXIED CONNECTION")
		}

		logFile, err := os.OpenFile(globals.LogDirectory, os.O_WRONLY|os.O_CREATE, 0644)
		globals.CheckError(err, 0)
		if globals.Silent {
			globals.Log.SetOutput(logFile)
		} else {
			mw := io.MultiWriter(os.Stdout, logFile)
			globals.Log.SetOutput(mw)
		}

		// --------------------------------------

		globals.Log.WithFields(logrus.Fields{
			"count": loopCount + 1,
			"total": globals.Runs,
		}).Infof("*** RUN START")

		err = Login(workingDirectory, insta)
		if err != nil {
			globals.Log.Error("login failed", err)
			globals.Log.Exit(1)
		}

		// print out important information about us before going to town
		meBefore, err := insta.Profiles.ByID(insta.Account.ID)
		globals.CheckError(err, 0)
		err = meBefore.Sync(true)
		globals.CheckError(err, 0)
		globals.Log.WithFields(logrus.Fields{
			"id":        meBefore.ID,
			"username":  meBefore.Username,
			"follower":  meBefore.FollowerCount,
			"following": meBefore.FollowingCount,
		}).Info("our stats before")

		if globals.RefreshTargets {
			// fetch & store all possible targets (to-be-followed users)
			Log.Debug("refreshing targets...")
			err = FetchTargets(globals.InfluencerUserNames, insta)
		}

		for loopCount < globals.Runs {
			// prepare fetched targets (to-be-followed users)
			err := PrepareTargets(insta)
			globals.CheckError(err, 0)
			globals.Log.WithFields(logrus.Fields{
				"step": "prepareTargets",
			}).Info("***")

			// try to follow all cleaned up targets and store target if successful
			err = GoFollow(insta)
			globals.CheckError(err, 0)
			globals.Log.WithFields(logrus.Fields{
				"step": "goFollow",
			}).Info("***")

			// --------------------------------------

			// wait until hopefully all newFollowings are following us
			if globals.DryRun != true {
				WaitMinutes(60, 180)
				globals.Log.WithFields(logrus.Fields{
					"step": "waitMinutes",
				}).Info("***")
			} else {
				globals.Log.WithFields(logrus.Fields{
					"step": "waitMinutes",
				}).Infof("[DRY RUN MODE]: not waiting!")
			}

			// --------------------------------------

			// try to unfollow all previously followed users and store target if successful
			err = GoUnfollow(insta)
			globals.CheckError(err, 0)
			globals.Log.WithFields(logrus.Fields{
				"step": "goUnfollow",
			}).Info("***")

			// print out important information about us after all is done
			meAfter, err := insta.Profiles.ByID(int64(insta.Account.ID))
			globals.CheckError(err, 0)
			err = meAfter.Sync(true)
			globals.CheckError(err, 0)
			globals.Log.WithFields(logrus.Fields{
				"id":                meAfter.ID,
				"username":          meAfter.Username,
				"follower":          meAfter.FollowerCount,
				"changed follower":  meAfter.FollowerCount - meBefore.FollowerCount,
				"following":         meAfter.FollowingCount,
				"changed following": meAfter.FollowingCount - meBefore.FollowingCount,
			}).Info("our stats after")

			// --------------------------------------

			globals.Log.SetOutput(os.Stderr)
			globals.Log.WithFields(logrus.Fields{
				"count": loopCount + 1,
				"total": globals.Runs,
			}).Infof("*** RUN DONE")

			if globals.DryRun != true {
				WaitMinutes(60, 180)
				globals.Log.WithFields(logrus.Fields{
					"step": "waitMinutes",
				}).Info("***")
			} else {
				globals.Log.WithFields(logrus.Fields{
					"step": "waitMinutes",
				}).Infof("[DRY RUN MODE]: not waiting!")
			}
			loopCount++
		}

		err = Cleanup(insta)
		globals.CheckError(err, 0)
		globals.Log.WithFields(logrus.Fields{
			"step": "cleanup",
		}).Infof("***")
	},
}

func init() {
	Log.Debug("init")

	var err error
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVarP(&globals.ConfigFile, "config", "c", globals.DefaultConfigFile, "config file")
	rootCmd.PersistentFlags().BoolVar(&globals.UseProxy, "use-proxy", globals.DefaultUseProxy, "use-proxy")
	rootCmd.PersistentFlags().BoolVar(&globals.DryRun, "dry-run", globals.DefaultDryRun, "dry-run")
	rootCmd.PersistentFlags().BoolVar(&globals.Silent, "silent", globals.DefaultSilent, "silent")
	rootCmd.PersistentFlags().BoolVar(&globals.RefreshTargets, "refresh-targets", globals.DefaultRefreshTargets, "refresh-targets")
	rootCmd.PersistentFlags().IntVar(&globals.Runs, "runs", globals.DefaultRuns, "number of runs")
	rootCmd.PersistentFlags().StringVar(&globals.UserName, "username", "", "userName")
	rootCmd.PersistentFlags().StringVar(&globals.Password, "password", "", "password")
	rootCmd.PersistentFlags().StringArrayVar(&globals.InfluencerUserNames, "influencer", []string{"one", "two"}, "influencer")

	err = viper.BindPFlag("config", rootCmd.PersistentFlags().Lookup("config"))
	err = viper.BindPFlag("use-proxy", rootCmd.PersistentFlags().Lookup("use-proxy"))
	err = viper.BindPFlag("dry-run", rootCmd.PersistentFlags().Lookup("dry-run"))
	err = viper.BindPFlag("silent", rootCmd.PersistentFlags().Lookup("silent"))
	err = viper.BindPFlag("refresh-targets", rootCmd.PersistentFlags().Lookup("refresh-targets"))
	err = viper.BindPFlag("runs", rootCmd.PersistentFlags().Lookup("runs"))
	err = viper.BindPFlag("username", rootCmd.PersistentFlags().Lookup("username"))
	err = viper.BindPFlag("password", rootCmd.PersistentFlags().Lookup("password"))
	err = viper.BindPFlag("influencer", rootCmd.PersistentFlags().Lookup("influencer"))

	if err != nil {
		os.Exit(1)
	}

	viper.SetDefault("config", globals.DefaultConfigFile)
	viper.SetDefault("use-proxy", globals.DefaultUseProxy)
	viper.SetDefault("refresh-targets", globals.DefaultRefreshTargets)
	viper.SetDefault("dry-run", globals.DefaultDryRun)
	viper.SetDefault("silent", globals.DefaultSilent)
	viper.SetDefault("runs", globals.DefaultRuns)

	// TODO:
	//rootCmd.AddCommand(loginCmd)
	//rootCmd.AddCommand(followCmd)

	// Cobra also supports local flags, which will only run
	// when this action is called directly.

	Log.Debug("init done!")
}

func initConfig() {
	Log.Debug("init config")
	// viper.SetDefault("username", globals.DefaultChannelName)
	// viper.SetDefault("password", globals.DefaultChannelName)

	viper.SetConfigType("json")
	viper.SetEnvPrefix("YGF")

	// Don't forget to read config either from cfgFile or from home directory!
	if globals.ConfigFile != "" {
		viper.SetConfigFile(globals.ConfigFile)
	} else {
		viper.SetConfigFile(globals.DefaultConfigFile)
	}

	viper.AutomaticEnv() // read in environment variables that match

	err := viper.ReadInConfig()
	// If a config file is found, read it in.
	if err == nil {
		Log.Info("Using config file: ", viper.ConfigFileUsed())
	} else {
		Log.Error("Error using config file:", err)
	}

	for idx, key := range viper.AllKeys() {
		//Log.Debugf("config > idx=%d key=%s value=%s", idx, key, viper.Get(key))
		Log.Debugf("config > idx=%d key=%s", idx, key)
	}

	Log.Info("All Keys: ", viper.AllKeys())

	globals.UseProxy = viper.GetBool("use-proxy")
	Log.Debugf(" > use-proxy: %t", globals.UseProxy)

	globals.RefreshTargets = viper.GetBool("refresh-targets")
	Log.Debugf(" > refresh-targets: %t", globals.RefreshTargets)

	globals.DryRun = viper.GetBool("dry-run")
	Log.Debugf(" > dry-run: %t", globals.DryRun)

	globals.Silent = viper.GetBool("silent")
	Log.Debugf(" > silent: %t", globals.Silent)

	globals.Runs = viper.GetInt("runs")
	Log.Debugf(" > runs: %d", globals.Runs)

	globals.UserName = viper.GetString("username")
	Log.Debugf(" > username: %s", globals.UserName)

	globals.Password = viper.GetString("password")
	Log.Debugf(" > password: %s", globals.Password)

	globals.InfluencerUserNames = viper.GetStringSlice("influencer")
	Log.Debug(" > influencer: ", globals.InfluencerUserNames)

	Log.Debug("init config done")
}

// Execute Executes is the entry point
func Execute() {
	Log.Debug("execute")

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}

	Log.Debug("execute done")
}
