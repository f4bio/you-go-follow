package dynamo

import (
  "context"
  "go.mongodb.org/mongo-driver/mongo"

  "f4b.io/you-go-follow/globals"
  "github.com/ahmdrz/goinsta/v2"
)

func login(workingDirectory string, insta *goinsta.Instagram, db *mongo.Database) error {
	globals.Log.Println("*** going to login")
	if err := insta.Login(); err != nil {
		globals.Log.Fatalln(err)
		return err
	}
	globals.Log.Println("*** logged in!", insta.Account.Username)

	// export your configuration
	if err := insta.Export(workingDirectory + "/" + globals.SessionFile); err != nil {
		globals.Log.Fatalln(err)
		return err
	}

	usersCollection := db.Collection(globals.DbCollectionUsers)

	{
		_, err := usersCollection.InsertOne(
			context.Background(),
			insta.Account,
			//[]interface{}{
			//  bson.NewDocument(
			//    bson.EC.Int64("userId", insta.Account.ID),
			//    bson.EC.String("fullName", insta.Account.FullName),
			//    bson.EC.String("username", insta.Account.Username),
			//  ),
			//},
		)
		if err != nil {
			globals.Log.Fatalln(err)
			return err
		}
	}
	return nil
}
