package dynamo

import (
  "f4b.io/you-go-follow/globals"
  "go.mongodb.org/mongo-driver/mongo"
  "go.mongodb.org/mongo-driver/mongo/options"
  "os"

  "github.com/ahmdrz/goinsta/v2"
  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/aws/session"
  "github.com/aws/aws-sdk-go/service/dynamodb"
  "github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
  "github.com/sirupsen/logrus"
  "github.com/thoas/go-funk"
)

var log = &logrus.Logger{
	Out: os.Stderr,
	Formatter: &logrus.TextFormatter{
		//TimestampFormat: "2018-10-03 20:13:37",
		FullTimestamp: true,
	},
	Hooks: make(logrus.LevelHooks),
	Level: logrus.DebugLevel,
}

func GetStorage(dbName string) *dynamodb.DynamoDB {
	// Initialize a session in us-west-2 that the SDK will use to load
	// credentials from the shared credentials file ~/.aws/credentials.
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("eu-central-1")},
	)
	CheckErr(err)
	// Create DynamoDB client
	svc := dynamodb.New(sess)
	globals.Log.Println("*** dynamodbclient:", svc)

	av, err := dynamodbattribute.MarshalMap(goinsta.Account{})
	CheckErr(err)
	globals.Log.Println("*** dynamodbattribute:", av)

	return svc
}

func GetMongoDb(dbName string) *mongo.Database {
  client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	CheckErr(err)

	db := client.Database(dbName)

	return db
}

// CheckError is a generic function to validate all errors.
func CheckErr(err error) {
	if err != nil {
		globals.Log.Fatalf("error: %s\n --- exiting ---", err)
		os.Exit(1337)
	}
}

// getMaxTargetCount return an "uncertain" maxTargetCount (try to make it more human)
func makeUncertain(inp int) int {
	return inp + funk.RandomInt(1, inp)
}
