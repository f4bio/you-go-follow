package cmd

import (
	"f4b.io/you-go-follow/globals"
	"f4b.io/you-go-follow/models"
	"github.com/ahmdrz/goinsta/v2"
	"github.com/jinzhu/gorm"
)

// GoFollow is the entry point
func GoFollow(insta *goinsta.Instagram) error {
	globals.Log.Println("[FOLLOW]: going to follow targets")

	var err error
	var db *gorm.DB
	var targets []models.InstaUser
	db = globals.GetStorage("working")

	db.Find(&targets)

	for targetUser := range targets {
		if globals.DryRun != true {
			globals.Log.Info("[FOLLOW]: going to follow:", targetUser)
			//err = targetUser.Follow()
			//if err != nil {
			//	globals.Log.Fatalln(err)
			//	continue
			//}

		} else {
			globals.Log.Info("[FOLLOW/DRY RUN MODE]: skipping following:", targetUser)
		}
		WaitSomeSeconds()
	}

	err = db.Close()
	if err != nil {
		Log.Fatalln("[FOLLOW]: unable to close database")
	}

	return nil
}
