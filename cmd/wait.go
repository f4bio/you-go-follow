package cmd

import (
  "f4b.io/you-go-follow/globals"
  "fmt"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/thoas/go-funk"
)

// WaitMinutes waits for random([low, high]) minutes
func WaitMinutes(low int, high int) {
	//globals.Log.Println("*** going to wait minutes: ***")

	rndDur, _ := time.ParseDuration(fmt.Sprintf("%dm", funk.RandomInt(low, high)))
	globals.Log.WithFields(logrus.Fields{
		"minutes": int(rndDur.Minutes()),
	}).Printf("sleeping...")
	ticker := time.NewTicker(1 * time.Minute)
	cntTicks := 0
	go func() {
		for range ticker.C {
			//globals.Log.WithFields(logrus.Fields{
			//  "ticksDone":  cntTicks,
			//  "ticksCount": int(rndDur.Minutes()),
			//}).Printf("still sleeping...")
			cntTicks++
		}
	}()
	time.Sleep(rndDur)
	ticker.Stop()

	//globals.Log.Println("\t *** :done waiting minutes ***")
}

// WaitSeconds waits for random([low, high]) seconds
func WaitSeconds(low int, high int) {
	//globals.Log.Println("*** going to wait seconds: ***")

	rnd := funk.RandomInt(low, high)
	rndDur, _ := time.ParseDuration(fmt.Sprintf("%ds", rnd))
	//globals.Log.WithFields(logrus.Fields{
	//  "seconds": int(rndDur.Seconds()),
	//}).Printf("sleeping...")
	time.Sleep(rndDur)

	//globals.Log.Println("\t *** :done waiting seconds ***")
}

// WaitSomeSeconds waits for random() seconds
func WaitSomeSeconds() {
	//globals.Log.Println("*** going to wait some seconds: ***")

	rnd := globals.Distribution[funk.RandomInt(0, len(globals.Distribution)-1)]
	rndDur, _ := time.ParseDuration(fmt.Sprintf("%ds", rnd))
	//globals.Log.WithFields(logrus.Fields{
	//  "seconds": int(rndDur.Seconds()),
	//}).Printf("sleeping...")
	time.Sleep(rndDur)

	//globals.Log.Println("\t *** :done waiting some seconds ***")
}
