package cmd

import (
	"f4b.io/you-go-follow/globals"
	"os"

	"github.com/ahmdrz/goinsta/v2"
	"github.com/tcnksm/go-input"
)

// Login logs in?
func Login(workingDirectory string, insta *goinsta.Instagram) error {
	var err error

	Log.Println("*** going to login")
	db := globals.GetStorage("users")

	if err = insta.Login(); err != nil {
		Log.Error("error logging in:", err)

		switch v := err.(type) {
		case goinsta.ChallengeError:
			err = insta.Challenge.Process(v.Challenge.APIPath)
			if err != nil {
				Log.Fatalln("[LOGIN]: unable to challenge", err)
			}

			ui := &input.UI{
				Writer: os.Stdout,
				Reader: os.Stdin,
			}

			query := "What is SMS code for instagram?"
			code, err := ui.Ask(query, &input.Options{
				Default:  "000000",
				Required: true,
				Loop:     true,
			})
			if err != nil {
				Log.Fatalln("[LOGIN]: unable to ask for sms code:", err)
			}

			err = insta.Challenge.SendSecurityCode(code)
			if err != nil {
				Log.Fatalln("[LOGIN]: unable to send security code:", err)
			}
		}
		Log.Fatalln(err)
	}
	Log.Println("*** logged in!", insta.Account.Username)

	// export your configuration
	if err := insta.Export(workingDirectory + "/" + globals.SessionFile); err != nil {
		Log.Fatalln("[LOGIN]: unable to export:", err)
		return err
	}
	db.Create(&insta.Account)
	err = db.Close()
	if err != nil {
		Log.Fatalln("[LOGIN]: unable to close database")
	}

	return nil
}
