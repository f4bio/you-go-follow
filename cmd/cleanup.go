package cmd

import (
  "f4b.io/you-go-follow/globals"
  "github.com/ahmdrz/goinsta/v2"
)

// Cleanup does something
func Cleanup(insta *goinsta.Instagram) error {
	globals.Log.Println("*** going to cleanup")

	//globals.Log.Println("logging out")
	err := insta.Logout()
	if err != nil {
		globals.Log.Fatalln(err)
		return err
	}
	return nil
}
