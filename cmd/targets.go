package cmd

import (
	"emperror.dev/errors"
	"encoding/json"
	"f4b.io/you-go-follow/globals"
	"f4b.io/you-go-follow/models"
	"github.com/jinzhu/gorm"
	"testing"

	"github.com/ahmdrz/goinsta/v2"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/thoas/go-funk"
)

// FetchTargets does something
func FetchTargets(starsUserNames []string, insta *goinsta.Instagram) error {
	globals.Log.Println("*** going to get targets")

	var err error
	var db *gorm.DB
	db = globals.GetStorage("targets")

	//targetsCollection := db.Collection(globals.DbCollectionTargets)
	targetsAddedCount := 0
	shuffledInfluencerUserNames := funk.ShuffleString(starsUserNames)

	globals.Log.Println("*** shuffled influencer:", shuffledInfluencerUserNames)

	for _, influencerUserName := range shuffledInfluencerUserNames {
		influencer, err := insta.Profiles.ByName(influencerUserName)

		globals.Log.Println("*** influencer:", influencer)

		if err != nil {
			globals.Log.Fatalln("error getting influencer profile:", err)
			return err
		}
		err = influencer.Sync(true)
		if err != nil {
			globals.Log.Fatalln("error syncing influencer profile:", err)
			return err
		}
		globals.Log.WithFields(logrus.Fields{
			"username":  influencer.Username,
			"id":        influencer.ID,
			"followers": influencer.FollowerCount,
			"following": influencer.FollowingCount,
		}).Info("influencer")

		starFollowers := influencer.Followers()
		for starFollowers.Next() {
			// TODO: check sometime if that's better (be careful because of RAM usage)
			//tmp := make([]interface{}, len(starFollowers.Users))
			//for i, v := range starFollowers.Users {
			// tmp[i] = v
			//}
			//{
			// _, err := targetsCollection.InsertMany(
			//   context.Background(),
			//   tmp,
			// )
			// CheckError(err)
			// //globals.Log.Debug("targets insert result=>", result)
			//}
			// TODO: or this (same reasons as above)
			//result := targetsCollection.FindOneAndReplace(
			//  context.Background(),
			//  bson.NewDocument(bson.EC.Int64("id", possibleTargetUser.ID)),
			//  possibleTargetUser,
			//)
			//if result == nil {
			//  targetsCollection.InsertOne(
			//    context.Background(),
			//    possibleTargetUser,
			//  )
			//}
			for _, possibleTargetUser := range starFollowers.Users {
				{
					//err := db.Write(globals.DbCollectionTargets, string(possibleTargetUser.ID), possibleTargetUser)
					db.Create(possibleTargetUser)
					if err != nil {
						globals.Log.Fatalln("insert error:", err)
					} else {
						targetsAddedCount += 1
					}
				}
			}
		}
		globals.Log.WithFields(logrus.Fields{
			"targetsAddedCount": targetsAddedCount,
		}).Info("Accumulated list of possible targets generated")
	}
	globals.Log.WithFields(logrus.Fields{
		"targetsAddedCount": targetsAddedCount,
	}).Info("Accumulated list of possible targets generated")

	err = db.Close()
	if err != nil {
		Log.Fatalln("[TARGETS]: unable to close database")
	}
	//globals.Log.Println("\t *** :done getting targets ***")
	return nil
}

func PrepareTargets(insta *goinsta.Instagram) error {
	globals.Log.Println("*** going to prepare targets")

	var err error
	var db *gorm.DB
	var newToFollow []models.InstaUser

	maxFollowingsCount := globals.MakeUncertain(globals.MaxFollowingsCount)
	followingsList := insta.Account.Following()
	followersList := insta.Account.Followers()

	db = globals.GetStorage("targets")

	db.Find(&newToFollow)

	for len(newToFollow) < maxFollowingsCount && len() > 0 {
		randIdx := int64(funk.RandomInt(0, targetsCount))
		randUser := goinsta.User{}
		err = json.Unmarshal([]byte(allTargets[randIdx]), &randUser)

		if err != nil {
			globals.Log.Error("error parsing possible target:", err)
			continue
		} else {
			globals.Log.Info("possible target:", randUser)
		}

		//result := targetsCollection.FindOne(
		//  context.TODO(),
		//	bson.NewDocument(
		//		bson.EC.SubDocumentFromElements("ID",
		//			bson.EC.Int64("$ne", insta.Account.ID),
		//		),
		//	),
		//	findopt.Skip(randIdx),
		//)
		//result := targetsCollection.FindOne(
		//	context.TODO(),
		//	bson.D{
		//		{"ID", bson.D{{"$ne", insta.Account.ID}}},
		//	})

		for followingsList.Next() {
			if funk.Contains(followingsList, randUser) {
				globals.Log.WithFields(logrus.Fields{
					"id":   randUser.ID,
					"name": randUser.FullName,
				}).Warnf("already following - skipping")
				continue
			}
		}

		if funk.Contains(followersList, randUser) {
			globals.Log.WithFields(logrus.Fields{
				"id":   randUser.ID,
				"name": randUser.FullName,
			}).Warnf("already follows us - skipping")
			continue
		}
		if funk.Contains(newToFollow, randUser) {
			globals.Log.WithFields(logrus.Fields{
				"id":   randUser.ID,
				"name": randUser.FullName,
			}).Warnf("already in target list - skipping")
			continue
		}
		newToFollow = append(newToFollow, randUser)
		addedCount++
		err := db.Write(globals.DbCollectionWorking, string(randUser.ID), randUser)
		if err != nil {
			Log.Fatalln(err)
			return err
		}
	}

	if len(newToFollow) == 0 {
		return errors.New("no possible targets could be prepared - quitting")
	}

	//newFollowings, err := goFollow(insta, newToFollow)
	//globals.Log.Infof("following '%d' (of '%d') new users", len(newFollowings), len(newToFollow))

	return nil
}

type MyMockedObject struct {
	mock.Mock
}

func TestGetTargets(t *testing.T) {
	// create an instance of our test object
	testObj := new(MyMockedObject)

	// setup expectations
	testObj.On("DoSomething", 123).Return(true, nil)

	// assert equality
	assert.Equal(t, 123, 123, "they should be equal")
	// assert inequality
	assert.NotEqual(t, 123, 456, "they should not be equal")
	// assert for nil (good for errors)
	assert.Nil(t, testObj)
	// assert for not nil (good when you expect something)
	if assert.NotNil(t, testObj) {
		// now we know that object isn't nil, we are safe to make
		// further assertions without causing any errors
		assert.Equal(t, "Something", testObj)
	}
}
